/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "player.h"

#include <cmath>

#include "cannon_projectile.h"
#include "game.h"
#include "projectile.h"


Player::Player(Game *game, qreal rmax) : Object(game, 100, PI / 2),
    rmax(rmax), primaryWeaponDeviation(0), primaryWeaponDelay(0),
	secondaryWeaponDelay(0)
{
    slideFriction = 0.8;
    rotFriction = 0.9750;

    slideAcc = 0.01;
    rotAcc = 0.4;
}

bool Player::isHittable() const
{
    return false;
}

/*
 * Determine in which direction the player should move.
 *
 * This method bases its calculations on the position of the mouse
 * pointer.
 */
void Player::recalculateActions()
{
    qreal mouseR = game->getMouseR();
    qreal mousePhi = game->getMousePhi();

    if (mouseR - r > 3) {
        moveAction = MoveBackward;
    } else if (r - mouseR > 3) {
        moveAction = MoveForward;
    } else {
        moveAction = DontMove;
    }

    if (fabs(mousePhi - phi) > 0.025) {
        if ((mousePhi < 0 && phi < 0) || (mousePhi > 0 && phi > 0)) {
            if (mousePhi - phi > 0) {
                slideAction = SlideLeft;
            } else {
                slideAction = SlideRight;
            }
        } else {
            if (fabs(mousePhi) + fabs(phi) > PI) {
                if (mousePhi < 0) {
                    slideAction = SlideLeft;
                } else {
                    slideAction = SlideRight;
                }
            } else {
                if (mousePhi < 0) {
                    slideAction = SlideRight;
                } else {
                    slideAction = SlideLeft;
                }
            }
        }
    } else {
        slideAction = DontSlide;
    }
}

/*
 * Move the player according to the position of the mouse pointer.
 */
void Player::control()
{
    recalculateActions();

    switch (moveAction) {
        case MoveForward:
            if (r > 100) {
                r *= 0.98;
            }
            break;
        case MoveBackward:
            if (r < rmax) {
                r *= 1.02;
            }
            break;
        case DontMove:
            break;
    }

    switch (slideAction) {
        case SlideLeft:
            slide = slide + slideAcc;
            break;
        case SlideRight:
            slide = slide - slideAcc;
            break;
        case DontSlide:
            break;
    }

    if (primaryWeaponDelay == 0 && game->isLMBPressed()) {
        primaryWeaponDelay = 10;
        firePrimaryWeapon();
    }

    if (secondaryWeaponDelay == 0 && game->isRMBPressed()) {
        secondaryWeaponDelay = 50;
        fireSecondaryWeapon();
    }

    if (primaryWeaponDelay > 0) {
        primaryWeaponDelay--;
    }

    if (secondaryWeaponDelay > 0) {
        secondaryWeaponDelay--;
    }
}

/*
 * Paint the contents of the player object.
 */
void Player::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                   QWidget *)
{
    QPixmap ship(":/images/spaceship.svg");
    painter->drawPixmap(-52, -40, 104, 60, ship);
}

void Player::firePrimaryWeapon()
{
    qreal projectileR = r - 50;
    qreal projectilePhi = phi + 0.01 * primaryWeaponDeviation;
    Projectile *projectile = new Projectile(game, projectileR, projectilePhi);

    projectile->setPos(projectileR * cos(projectilePhi),
        projectileR * sin(projectilePhi));
    projectile->rotate(this->angle - 4.0 + 8.0 * rand() / RAND_MAX);

    if (primaryWeaponDeviation == 3) {
        primaryWeaponDeviation = 0;
    } else {
        primaryWeaponDeviation++;
    }

    game->scheduleSpawn(projectile);
}

void Player::fireSecondaryWeapon()
{
    qreal projectileR = r - 50;
    CannonProjectile *projectile = new CannonProjectile(game, projectileR,
        phi);

    projectile->setPos(projectileR * cos(phi), projectileR * sin(phi));
    projectile->rotate(this->angle - 0.5 + 1.0 * rand() / RAND_MAX);

    game->scheduleSpawn(projectile);
}
