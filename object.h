/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef OBJECT_H
#define OBJECT_H

#include <QGraphicsItem>

#define PI 3.141592653589793238


class Game;


/*
 * Base class for all kinds of game objects.
 *
 * Object defines the interface all game objects should adhere to, and
 * provides default implementations of some of its methods.
 */
class Object : public QGraphicsItem
{
public:
    Object(Game *game, qreal r, qreal phi);

    virtual void control();
    virtual void physics();
    virtual void step();

    QRectF boundingRect() const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option,
               QWidget *widget);

    // true, if the object explodes on collision, false otherwise
    virtual bool isExplosive() const;
    // true, if the object can be hit, false otherwise
    virtual bool isHittable() const;
    // Subtract damage from the object's health
    virtual void hit(int damage);

    // Accessor methods
    int getValue() const;
    virtual bool isAlive() const;

    qreal getR() const;
    void setR(qreal r);

    qreal getPhi() const;
    void setPhi(qreal r);

    qreal getSlide() const;
    void setSlide(qreal slide);

protected:
    Game *game;

    /*
     * Hitpoints and the value which is added to the player's score
     * when they destroy the object.
     */
    int health;
    int value;

    // Position expressed in polar coordinates
    qreal r;
    qreal phi;

    qreal angle;

    // Sidewise movement speed, acceleration and friction
    qreal slide;
    qreal slideAcc;
    qreal slideFriction;

    // Rotation speed, acceleration and friction
    qreal rot;
    qreal rotAcc;
    qreal rotFriction;
};

#endif // OBJECT_H
