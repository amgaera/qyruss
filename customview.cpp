/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "customview.h"

#include <QMouseEvent>


CustomView::CustomView(QWidget *parent) : QGraphicsView(parent), x(0), y(0),
    rmbPressed(false), lmbPressed(false)
{
}

int CustomView::getX() const
{
    return x;
}

int CustomView::getY() const
{
    return y;
}

bool CustomView::isRMBPressed() const
{
    return rmbPressed;
}

bool CustomView::isLMBPressed() const
{
    return lmbPressed;
}

void CustomView::mouseMoveEvent(QMouseEvent *event)
{
    x = event->x();
    y = event->y();
    event->accept();
}

void CustomView::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons().testFlag(Qt::LeftButton)) {
        lmbPressed = true;
    }

    if (event->buttons().testFlag(Qt::RightButton)) {
        rmbPressed = true;
    }

    event->accept();
}

void CustomView::mouseReleaseEvent(QMouseEvent *event)
{
    lmbPressed = false;
    rmbPressed = false;

    event->accept();
}
