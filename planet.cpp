/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <QPainter>

#include "planet.h"


Planet::Planet(Game *game) : Object(game, 0, 0)
{
}

bool Planet::isExplosive() const
{
    return false;
}

bool Planet::isHittable() const
{
    return false;
}

void Planet::control()
{
}

void Planet::physics()
{
}

void Planet::step()
{
}

/*
 * Paint the contents of the planet.
 */
void Planet::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                   QWidget *)
{
    painter->setPen(Qt::NoPen);

    QRadialGradient *grad = new QRadialGradient(0, 0, 40);
    grad->setColorAt(0, QColor(147, 39, 36, 255));
    grad->setColorAt(0.33, QColor(225, 169, 95, 255));
    grad->setColorAt(0.67, QColor(225, 169, 95, 155));
    grad->setColorAt(1, QColor(150, 75, 0, 55));

    QBrush brush(*grad);
    painter->setBrush(brush);
    painter->drawEllipse(-40, -40, 80, 80);
}
