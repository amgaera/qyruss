/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef GAME_H
#define GAME_H

#include <QMutex>
#include <QQueue>
#include <QTimer>

#include "customview.h"
#include "hud.h"
#include "object.h"
#include "player.h"


/*
 * Class implementing the game's logic.
 *
 * This class manages the scene on which all game objects are drawn,
 * spawns and destroys game objects, and provides a game loop which
 * calls on objects to animate and move themselves.
 */
class Game : public QObject
{
    Q_OBJECT
public:
    Game(CustomView *view);

    void start();
    bool isOver() const;

    // Getters returning the polar coordinates of the mouse pointer
    qreal getMouseR() const;
    qreal getMousePhi() const;

    // Getters returning the state of the two mouse buttons
    bool isRMBPressed() const;
    bool isLMBPressed() const;

    void scheduleSpawn(Object *object);

private:
    HUD *hud;
    Player *player;

    /*
     * Objects farther away from the center of the viewport than this
     * get deleted.
     */
    qreal maxRadius;

    CustomView *view;
    QTimer mainTimer;
    QGraphicsScene scene;

    QQueue<Object*> spawnQueue;
    QQueue<Object*> destroyQueue;

    QMutex gameLoopMutex;
    bool gameOver;

    // Processing helpers for the game's main loop
    void handleDeadObject(Object *object);
    void handleEscapedObject(Object *object);
    void handleExplosiveObject(Object *object);
    void handleHittableObject(Object *object);

    // Additional helper methods
    void endGame();
    void spawnNewEnemies();
    void processObjectQueues();

private slots:
    void mainClockTick();
};

#endif // GAME_H
