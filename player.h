/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PLAYER_H
#define PLAYER_H

#include "object.h"


/*
 * Class representing the player.
 */
class Player : public Object
{
public:
    Player(Game *game, qreal rmax);
    bool isHittable() const;
    void control();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    enum SlideAction { DontSlide, SlideLeft, SlideRight };
    enum MoveAction { DontMove, MoveForward, MoveBackward };

    // Fields controlling in which direction the player will move
    SlideAction slideAction;
    MoveAction moveAction;

    // The player's maximum distance from the center of the viewport
    qreal rmax;

    // State of the player's weapons
    int primaryWeaponDeviation;
    int primaryWeaponDelay;
    int secondaryWeaponDelay;

    // Helper methods
    void firePrimaryWeapon();
    void fireSecondaryWeapon();
    void recalculateActions();
};

#endif // PLAYER_H
