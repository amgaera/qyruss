/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef STAR_H
#define STAR_H

#include "object.h"


/*
 * Class representing stars.
 *
 * Stars are created by the game automatically to make the background
 * appear dynamic to the player. They don't interact with other objects
 * and are deleted as soon as they leave the game's visible area.
 */
class Star : public Object
{
public:
    Star(Game *game);

    bool isHittable() const;
    bool isExplosive() const;

    void control();
    void physics();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
};

#endif // STAR_H
