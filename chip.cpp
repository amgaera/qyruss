/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "chip.h"

#include <QPainter>
#include <QVarLengthArray>


Chip::Chip(Game *game, qreal r, qreal phi) : Object(game, r, phi),
    color(qrand() % 256, qrand() % 256, qrand() % 256)
{
    value = 100;
    health = 250;
    clockwise = true;

    scale(0.5, 0.5);
}

QRectF Chip::boundingRect() const
{
    return QRect(10, 10, 85, 85);
}

void Chip::hit(int damage)
{
    health -= damage;
}

/*
 * Paint the contents of the chip.
 */
void Chip::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setBrush(color);
    painter->drawRect(QRect(14, 14, 79, 39));

    painter->setPen(QPen(Qt::gray, 1));
    painter->drawLine(15, 54, 94, 54);
    painter->drawLine(94, 53, 94, 15);
    painter->setPen(QPen(Qt::white, 0));

    // Draw lines
    QVarLengthArray<QLineF, 36> lines;

    for (int i = 0; i <= 10; i += 1) {
        lines.append(QLineF(18 + 7 * i, 13, 18 + 7 * i, 5));
        lines.append(QLineF(18 + 7 * i, 54, 18 + 7 * i, 62));
    }
    for (int i = 0; i <= 6; i += 1) {
        lines.append(QLineF(5, 18 + i * 5, 13, 18 + i * 5));
        lines.append(QLineF(94, 18 + i * 5, 102, 18 + i * 5));
    }

    const QLineF lineData[] = {
        QLineF(25, 35, 35, 35),
        QLineF(35, 30, 35, 40),
        QLineF(35, 30, 45, 35),
        QLineF(35, 40, 45, 35),
        QLineF(45, 30, 45, 40),
        QLineF(45, 35, 55, 35)
    };
    lines.append(lineData, 6);

    painter->drawLines(lines.data(), lines.size());
}

void Chip::control()
{
    // Randomly change direction
    if (qrand() % 100 == 0) {
        clockwise = !clockwise;
    }
    else {
        if (clockwise) {
            phi -= 0.005;
        }
        else {
            phi += 0.005;
        }
    }

    r *= 1.0025;
}
