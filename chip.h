/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CHIP_H
#define CHIP_H

#include "object.h"


/*
 * Class representing chips.
 *
 * Chips are the spaceship's advanced enemies, i.e. they're much more
 * resilient and worth more points than mice. They're created
 * automatically by the game and can be destroyed by firing projectiles
 * at them.
 */
class Chip : public Object
{
public:
    Chip(Game *game, qreal r, qreal phi);

    void control();
    void hit(int damage);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    QColor color;
    bool clockwise;
};

#endif // CHIP_H
