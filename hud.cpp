/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "hud.h"

#include <QPainter>


HUD::HUD(Game *game) : Object(game, 0, 0), score(0), energy(500),
    gameOver(false)
{
}

bool HUD::isExplosive() const
{
    return false;
}

bool HUD::isHittable() const
{
    return false;
}

int HUD::getEnergy() const
{
    return energy;
}

void HUD::decEnergy(int damage)
{
    energy -= damage;
}

void HUD::incScore(int points)
{
    score += points;
}

void HUD::setGameOver(bool gameOver)
{
    this->gameOver = gameOver;
}

void HUD::control()
{
}

void HUD::physics()
{
}

void HUD::step()
{
}

/*
 * Paint the contents of the HUD.
 */
void HUD::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(QColor(0, 255, 0));
    painter->drawText(0, 0, QString::number(score).prepend("Score: "));
    painter->setPen(QColor(255, 0, 0));
    painter->drawText(575, 0, QString::number(energy).prepend("Energy: "));

    if (gameOver) {
        painter->setPen(QColor(255, 255, 0));
        painter->drawText(185, 0, "Game Over. Press 'e' for a new game.");
    }
}
