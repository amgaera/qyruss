/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "object.h"


/*
 * Class representing projectiles, i.e. objects that cause damage when
 * collided with.
 */
class Projectile : public Object
{
public:
    Projectile(Game *game, qreal r, qreal phi);

    bool isExplosive() const;
    bool isHittable() const;
    bool isAlive() const;

    void control();
    void physics();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

protected:
    int lifeTimer;
};

#endif // PROJECTILE_H
