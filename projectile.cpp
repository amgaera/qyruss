/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "projectile.h"

#include <QPainter>


/*
 * Create a new projectile and place it on the given position.
 *
 * The position is specified in polar coordinates.
 */
Projectile::Projectile(Game *game, qreal r, qreal phi) : Object(game, r, phi),
    lifeTimer(100)
{
    slideFriction = 0.96;
    rotFriction = 0.98;
}

/*
 * Return the projectile's bounding rectangle.
 */
QRectF Projectile::boundingRect() const
{
    return QRectF(-1, 0, 1, 10);
}

void Projectile::physics()
{
}

bool Projectile::isExplosive() const
{
    return true;
}

bool Projectile::isHittable() const
{
    return false;
}

bool Projectile::isAlive() const
{
    return lifeTimer > 0 && r > 50;
}

void Projectile::control()
{
    lifeTimer--;
    r -= 4;
}

/*
 * Paint the contents of the projectile.
 */
void Projectile::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                       QWidget *)
{
    QPen primaryPen(QColor(255, 50, 0), 1.0, Qt::SolidLine, Qt::RoundCap);
    painter->setPen(primaryPen);
    painter->drawLine(0, 10, 0, 0);

    QPen secondaryPen(QColor(255, 225, 175), 0);
    painter->setPen(secondaryPen);
    painter->drawLine(0, 8, 0, 0);
}
