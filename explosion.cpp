/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "explosion.h"

#include <cmath>

#include <QPainter>


Explosion::Explosion(Game *game, int size, QString text, QColor colour) :
    Object(game, 0, 0), size(size), lifeTimer(size), lifeMax(size), text(text),
    colour(colour)
{
}

Explosion::Explosion(Game *game, int size, int time, QString text,
                     QColor colour) : Object(game, 0, 0), size(size),
    lifeTimer(time), lifeMax(time), text(text), colour(colour)
{
}

void Explosion::control()
{
    lifeTimer--;
}

void Explosion::physics()
{
}

void Explosion::step()
{
}

bool Explosion::isHittable() const
{
    return false;
}

bool Explosion::isAlive() const
{
    return lifeTimer > 0;
}

/*
 * Paint the contents of the explosion.
 */
void Explosion::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                      QWidget *)
{
    QFont font = QFont(painter->font());

    painter->setPen(colour);
    font.setPointSize(5 * log(lifeMax));
    painter->setFont(font);

    painter->drawText(0, 0, text);
    painter->setPen(Qt::NoPen);

    // Current size of the fireball
    qreal currentSize = size * (lifeMax - lifeTimer) / (lifeMax);
    QRadialGradient grad(0, 0, currentSize);

    // Where should the outer ring start dissapearing
    int vanish = (255 * lifeTimer) / lifeMax;
    grad.setColorAt(0, QColor(255, vanish, 0, vanish));
    grad.setColorAt(1, QColor(0, 0, 0, 0));

    QBrush brush(grad);
    painter->setBrush(brush);
    painter->drawEllipse(-currentSize, -currentSize, currentSize * 2,
                         currentSize * 2);

    // If the explosion's grown enough, draw additional rings
    if (size > 25) {
        // Modify the wave's size according to its age
        qreal wave = 2 * (lifeMax - lifeTimer) - 7 * sqrt(lifeMax - lifeTimer);
        // Transparency values of the wave in key points
        qreal alfa1, alfa2;

        if (lifeTimer > 30) {
            alfa1 = 150;
            alfa2 = 30;
        } else {
            // The wave starts to dissapear
            alfa1 = 5 * lifeTimer;
            alfa2 = lifeTimer;
        }

        QRadialGradient radialGrad(QPointF(0, 0), wave, QPointF(0, 0));

        radialGrad.setColorAt(0.0, QColor(0, 0, 0, 0));
        radialGrad.setColorAt(0.7, QColor(180, 180, 180, 0));
        radialGrad.setColorAt(0.9, QColor(180, 180, 180, alfa1));
        radialGrad.setColorAt(1, QColor(180, 180, 180, alfa2));

        painter->setBrush(radialGrad);
        painter->drawEllipse(-wave, -wave, 2 * wave, 2 * wave);
    }
}
