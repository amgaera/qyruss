# -------------------------------------------------
# Project created by QtCreator 2010-06-19T16:13:12
# -------------------------------------------------
TARGET = qyruss
TEMPLATE = app
SOURCES += main.cpp \
    mouse.cpp \
    object.cpp \
    window.cpp \
    player.cpp \
    projectile.cpp \
    customview.cpp \
    explosion.cpp \
    planet.cpp \
    star.cpp \
    hud.cpp \
    chip.cpp \
    cannon_projectile.cpp \
    game.cpp
HEADERS += mouse.h \
    object.h \
    window.h \
    player.h \
    projectile.h \
    customview.h \
    explosion.h \
    planet.h \
    star.h \
    hud.h \
    chip.h \
    cannon_projectile.h \
    game.h
FORMS += mainwindow.ui
RESOURCES += qyruss.qrc
