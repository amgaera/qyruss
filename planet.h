/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PLANET_H
#define PLANET_H

#include "object.h"


/*
 * Class representing the planet.
 *
 * Exactly one planet objects is created by the game. It is placed in the
 * center of the viewport and represents the spaceships destination.
 */
class Planet : public Object
{
public:
    Planet(Game *game);

    bool isHittable() const;
    bool isExplosive() const;

    void control();
    void physics();
    void step();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
};

#endif // PLANET_H
