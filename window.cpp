/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "window.h"


Window::Window(QWidget *parent) : QMainWindow(parent)
{
    setupUi(this);
    qsrand(QTime(0, 0, 0).secsTo(QTime::currentTime()));

    game = new Game(view);
    game->start();
}

void Window::keyPressEvent(QKeyEvent *event)
{
    /*
     * The only key we accept is 'e', and we only expect it if the
     * player has lost the game and wants to start a new one.
     */
    if (!game->isOver() || event->key() != Qt::Key_E) {
        event->ignore();
        return;
    }

    game->start();
    event->accept();
}

/*
 * Handle resize events by scaling the window's contents.
 */
void Window::resizeEvent(QResizeEvent *event)
{
    if (event->oldSize().width() > 10) {
        view->scale(qreal(event->size().width()) / event->oldSize().width(),
                    qreal(event->size().height()) / event->oldSize().height());
    }

    event->accept();
}
