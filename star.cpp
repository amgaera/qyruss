/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "star.h"

#include <cmath>

#include <QPainter>


Star::Star(Game *game) : Object(game, 45, 2 * PI * (qrand() % 360) / 360)
{
}

bool Star::isExplosive() const
{
    return false;
}

bool Star::isHittable() const
{
    return false;
}

void Star::control()
{
    r *= 1.03;
}

void Star::physics()
{
}

void Star::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPolygonF starPolygon;

    starPolygon << QPointF(1.0, 0.5);

    for (int i = 1; i < 5; ++i) {
        starPolygon << QPointF(0.5 + 0.5 * cos(0.8 * i * 3.14),
                               0.5 + 0.5 * sin(0.8 * i * 3.14));
    }

    painter->scale(3, 3);

    painter->setPen(Qt::NoPen);
    painter->setBrush(QColor(255, 255, 255));

    painter->drawPolygon(starPolygon, Qt::WindingFill);
}
