/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "object.h"

#include <cmath>

#include <QPainter>


Object::Object(Game *game, qreal r, qreal phi): game(game), health(1),
    value(0), r(r), phi(phi), angle(0), slide(0), slideAcc(0.08),
    slideFriction(0.9850), rot(0), rotAcc(0.5), rotFriction(0.9800)
{
}

int Object::getValue() const
{
    return value;
}

bool Object::isAlive() const
{
    return health > 0;
}

qreal Object::getR() const
{
    return r;
}

void Object::setR(qreal r)
{
    this->r = r;
}

qreal Object::getPhi() const
{
    return phi;
}

void Object::setPhi(qreal phi)
{
    this->phi = phi;
}

qreal Object::getSlide() const
{
    return slide;
}

void Object::setSlide(qreal slide)
{
    this->slide = slide;
}

QRectF Object::boundingRect() const
{
    return QRectF(-10, -10, 20, 20);
}

bool Object::isExplosive() const
{
    return false;
}

bool Object::isHittable() const
{
    return true;
}

void Object::hit(int)
{
}

/*
 * Recalculate the object's non-physical properties.
 *
 * For example the object's life timer may be decreased here.
 */
void Object::control()
{
}

/*
 * Recalculate the object's physical properties.
 *
 * This method finds new values for the rot and slide fields, taking
 * into account the acceleration and friction forces influencing them.
 */
void Object::physics()
{
    double arc = atan2(-pos().x(), pos().y()) * 180.0 / PI;

    if (arc < 0) {
        arc += 360;
    }

    double diff = angle - arc;

    if (diff < -180) {
        diff += 360;
    } else if (diff > 180) {
        diff -= 360;
    }

    if (diff > -1 && diff < 1) {
        if (rot >= 0.025 || rot <= -0.025) {
            rot = rot * rotFriction;
        } else {
            rot = 0;
        }
    } else if (diff > 1 && rot > 0 && rot / diff > 0.185) {
        rot = rot - rotAcc;
    } else if (diff < -1 && rot < 0 && rot / diff > 0.185) {
        rot = rot + rotAcc;
    } else if (diff > 0) {
        rot = rot + rotAcc;
    } else {
        rot = rot - rotAcc;
    }

    if (slide >= 0.001 || slide <= -0.001) {
        slide = slide * slideFriction;
    } else {
        slide = 0;
    }
}

/*
 * Update the object's position.
 *
 * This method sets the object's position based on its polar
 * coordinates. Moreover, it sanitises several of its other properties.
 */
void Object::step()
{
    rotate(-rot);
    angle -= rot;
    phi += slide;

    if (angle < 0) angle += 360;
    if (angle > 360) angle -= 360;

    if (phi < -PI) phi = phi + 2 * PI;
    else if (phi > PI) phi = phi - 2 * PI;

    setScale(log10(r / 25));
    setPos(r * cos(phi), r * sin(phi));
}

/*
 * Paint the object's contents.
 */
void Object::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                   QWidget *)
{
    painter->setBrush(Qt::gray);
    painter->drawRect(-10, -10, 20, 20);
}
