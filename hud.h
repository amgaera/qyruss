/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HUD_H
#define HUD_H

#include "object.h"


/*
 * Class representing the game's head-up display.
 *
 * HUD displays information such as the player's current score and
 * remaining energy.
 */
class HUD : public Object
{
public:
    HUD(Game *game);

    bool isHittable() const;
    bool isExplosive() const;

    void control();
    void physics();
    void step();

    // Accessor methods
    int getEnergy() const;
    void decEnergy(int damage);
    void incScore(int points);
    void setGameOver(bool gameOver = true);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    int score;
    int energy;
    bool gameOver;
};

#endif // HUD_H
