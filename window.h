/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef WINDOW_H
#define WINDOW_H

#include <QtGui>

#include "game.h"
#include "ui_mainwindow.h"


/*
 * Class representing the window in which the game is displayed.
 */
class Window : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = 0);

private:
    Game *game;

public slots:
    void keyPressEvent(QKeyEvent *event);
    void resizeEvent(QResizeEvent * event);
};

#endif // WINDOW_H
