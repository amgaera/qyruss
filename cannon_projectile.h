/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CANNONPROJECTILE_H
#define CANNONPROJECTILE_H

#include "projectile.h"


/*
 * Class representing cannon projectiles, i.e. projectiles that cause
 * more damage, but can't be fire as often as regular projectiles.
 */
class CannonProjectile : public Projectile
{
public:
    CannonProjectile(Game *game, qreal r, qreal phi);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
};

#endif // CANNONPROJECTILE_H
