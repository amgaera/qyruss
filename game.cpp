/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "game.h"

#include <cmath>

#include "chip.h"
#include "explosion.h"
#include "mouse.h"
#include "planet.h"
#include "star.h"


Game::Game(CustomView *view)
{
    // Scene setup
    scene.setSceneRect(-395, -300, 790, 600);
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    scene.installEventFilter(this);

    // View setup
    this->view = view;
    view->setBackgroundBrush(QBrush(Qt::black));
    view->setScene(&scene);

    // Timer setup
    connect(&mainTimer, SIGNAL(timeout()), this, SLOT(mainClockTick()));
    mainTimer.start(30);

    maxRadius = 0.9 * (view->height() / 2.0);
}

/*
 * Start a new game.
 *
 * This method clears the scene, creates all required game objects
 * (e.g. the planet and the player) and starts processing them.
 */
void Game::start()
{
    foreach (QGraphicsItem *item, scene.items()) {
        scene.removeItem(item);
        delete item;
    }

    Planet *planet = new Planet(this);
    planet->setPos(0, 0);
    scene.addItem(planet);

    hud = new HUD(this);
    hud->setPos(-325, -320);
    scene.addItem(hud);

    player = new Player(this, maxRadius - 5);
    player->setPos(100, 100);
    scene.addItem(player);

    gameOver = false;
}

bool Game::isOver() const
{
    return gameOver;
}

qreal Game::getMouseR() const
{
    double x = view->getX() - view->width() / 2.0;
    double y = view->getY() - view->height() / 2.0;

    return sqrt(pow(x, 2) + pow(y, 2));
}

qreal Game::getMousePhi() const
{
    double x = view->getX() - view->width() / 2.0;
    double y = view->getY() - view->height() / 2.0;

    return atan2(y, x);
}

bool Game::isRMBPressed() const
{
    return view->isRMBPressed();
}

bool Game::isLMBPressed() const
{
    return view->isLMBPressed();
}

void Game::scheduleSpawn(Object *object)
{
    spawnQueue.enqueue(object);
}

/*
 * The game's main loop.
 */
void Game::mainClockTick()
{
    /*
     * Check if the previous iteration of the game loop has ended
     * already.
     */
    if (!gameLoopMutex.tryLock()) {
        return;
    }

    foreach (QGraphicsItem *item, scene.items()) {
        Object *object = dynamic_cast<Object *> (item);

        // Let the object recalculate and update its position
        object->control();
        object->physics();
        object->step();

        /*
         * Specialised handlers take care of objects that require our
         * attention.
         */
        if (!object->isAlive()) {
            handleDeadObject(object);
        } else if (object->getR() > maxRadius) {
            handleEscapedObject(object);
        } else if (object->isExplosive()) {
            handleExplosiveObject(object);
        } else if (!gameOver && object->isHittable()) {
            handleHittableObject(object);
        }
    }

    if (!gameOver && hud->getEnergy() <= 0) {
        endGame();
    }

    spawnNewEnemies();
    processObjectQueues();

    scene.update();
    gameLoopMutex.unlock();
}

/*
 * Remove dead objects.
 */
void Game::handleDeadObject(Object *object)
{
    int quadValue = 4 * object->getValue();

    if (quadValue) {
        Explosion* expl = new Explosion(this, int(object->getR() / 3),
                                        QString::number(quadValue),
                                        QColor(255, 255, 0));
        expl->setPos(object->scenePos());

        spawnQueue.enqueue(expl);
    }

    hud->incScore(quadValue);
    destroyQueue.enqueue(object);
}

/*
 * Remove objects that travelled outside the viewport.
 */
void Game::handleEscapedObject(Object *object)
{
    if (object->getValue() > 0) {
        if (!gameOver) {
            hud->incScore(-2 * object->getValue());
        }

        Explosion* expl = new Explosion(this, 1, 20,
                                        QString::number(-2 * object->getValue()),
                                        QColor(255, 0, 0));

        expl->setPos(object->scenePos());
        spawnQueue.enqueue(expl);
    }

    destroyQueue.enqueue(object);
}

/*
 * Generate explosions if explosive objects collide with other objects.
 */
void Game::handleExplosiveObject(Object *object)
{
    foreach(QGraphicsItem *item, scene.items()) {
        Object *target = dynamic_cast<Object *> (item);

        if (target->isHittable() && object->collidesWithItem(target)) {
            target->hit(25);
            hud->incScore(target->getValue());

            Explosion* expl = new Explosion(this,
                                            1 + int(target->getR() / 10),
                                            QString::number(target->getValue()),
                                            QColor(0, 255, 0));
            expl->setPos(object->scenePos());

            spawnQueue.enqueue(expl);
            destroyQueue.enqueue(object);
            break;
        }
    }
}

/*
 * Bounce colliding hittable objects off the player.
 */
void Game::handleHittableObject(Object *object)
{
    if (object->collidesWithItem(player)) {
        hud->decEnergy(50);
        player->setSlide(-player->getSlide() - 0.05);
        object->hit(25);
        object->setR(object->getR() - 10);
    }
}

/*
 * Destroy the player object and display the "Game Over" screen.
 */
void Game::endGame()
{
    Explosion* expl = new Explosion(this, 100, "Game Over", QColor(0, 0, 255));
    expl->setPos(player->scenePos());

    spawnQueue.enqueue(expl);
    destroyQueue.enqueue(player);

    gameOver = true;
    hud->setGameOver();
}

/*
 * Spawn new mice, chips and stars.
 */
void Game::spawnNewEnemies()
{
    if (qrand() % 150 == 0) {
        Mouse *mouse = new Mouse(this, 50, 2 * PI * qrand() / RAND_MAX);
        scene.addItem(mouse);
        mouse->rotate(180);
        mouse->step();
    }

    if (qrand() % 300 == 0) {
        Chip *chip = new Chip(this, 50, 2 * PI * qrand() / RAND_MAX);
        scene.addItem(chip);
        chip->rotate(90);
        chip->step();
    }

    if (qrand() % 5 == 0) {
        Star *star = new Star(this);
        scene.addItem(star);
        star->step();
    }
}

/*
 * Process spawnQueue and destroyQueue.
 */
void Game::processObjectQueues()
{
    while(!spawnQueue.empty()) {
        scene.addItem(spawnQueue.dequeue());
    }

    while(!destroyQueue.empty()) {
        Object *dead = destroyQueue.dequeue();

        // Check if the object has already been removed from the scene
        if (&scene == dead->scene()) {
            scene.removeItem(dead);
        }

        delete dead;
    }
}
