About
=====

Qyruss is a simple shoot 'em up game inspired by the video arcade game
Gyruss. It's written in C++ using Qt.

Dependencies
============

Qyruss only requires Qt version 4 to build. It should work with any
recent release of that framework.

Compilation
===========

You can compile and run Qyruss with the following commands::

        qmake
        make
        ./qyruss

Alternatively, you can use Qt Creator to open, compile, and run the
project.

Screenshots
===========

.. image:: https://lh6.googleusercontent.com/-LO_8FUkNlGs/UYrHgnyvUsI/AAAAAAAAANU/JrF9epS7nhA/w662-h663-no/qyruss2.png
