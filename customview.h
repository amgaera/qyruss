/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CUSTOMVIEW_H
#define CUSTOMVIEW_H

#include <QGraphicsView>


/*
 * Custom QGraphicsView subclass that stores mouse state.
 *
 * This class extends QGraphicsView to provide methods to query the
 * position and state of the mouse.
 */
class CustomView : public QGraphicsView
{
    Q_OBJECT
public:
    CustomView(QWidget *parent = 0);

    int getX() const;
    int getY() const;

    bool isRMBPressed() const;
    bool isLMBPressed() const;

private:
    int x;
    int y;
    bool rmbPressed;
    bool lmbPressed;

public slots:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent * event);
};

#endif // CUSTOMVIEW_H
