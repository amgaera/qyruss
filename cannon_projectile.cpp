/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "cannon_projectile.h"

#include <QPainter>


/*
 * Create a cannon projectile and place it on the given position.
 *
 * The position is specified in polar coordinates.
 */
CannonProjectile::CannonProjectile(Game *game, qreal r, qreal phi) :
    Projectile(game, r, phi)
{
    slideFriction = 0.96;
    rotFriction = 0.98;

    lifeTimer = 175;
}

/*
 * Paint the contents of the cannon projectile.
 */
void CannonProjectile::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen primary_pen(QColor(255, 100, 0), 2.5, Qt::SolidLine, Qt::RoundCap);
    painter->setPen(primary_pen);
    painter->drawLine(0, 15, 0, 0);

    QPen secondary_pen(QColor(255, 200, 125), 0);
    painter->setPen(secondary_pen);
    painter->drawLine(0, 12, 0, 0);
}
